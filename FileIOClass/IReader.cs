﻿namespace FileIOClass
{
    public interface IReader<T>
    {
        public string Path { get; set; }
        IEnumerable<T> GetValues();
    }
}
