﻿using CsvHelper;
using FileIOClass.Models;
using System.Globalization;

namespace FileIOClass
{
    public class CsvGuitarFileReader : IReader<Guitar>
    {
        public string Path { get; set; } = string.Empty;

        public IEnumerable<Guitar> GetValues()
        {
            using var reader = new StreamReader(Path);
            using var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture);
            return csvReader.GetRecords<Guitar>().ToList();
        }
    }
}
