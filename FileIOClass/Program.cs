﻿using FileIOClass;
using FileIOClass.Models;

var writer = new CsvGuitarFileWriter { Path = "guitars.csv" };

writer.Rewrite(new Guitar[] {
    new Guitar { Brand = "Fender", Model = "Strat", YearManufactured = "1968" },
    new Guitar { Brand = "Fender", Model = "Telecaster", YearManufactured = "1968" }
});

var reader = new CsvGuitarFileReader { Path = "guitars.csv" };

var guitars = reader.GetValues();

foreach (var guitar in guitars)
{
    Console.WriteLine(guitar.Model);
}
