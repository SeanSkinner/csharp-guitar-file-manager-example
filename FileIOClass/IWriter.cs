﻿namespace FileIOClass
{
    public interface IWriter<T>
    {
        public string Path { get; set; }
        void Append(T entry);
        void Append(IEnumerable<T> entries);
        void Rewrite(T entry);
        void Rewrite(IEnumerable<T> entries);
        void Clear();
    }
}
