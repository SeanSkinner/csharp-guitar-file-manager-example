﻿using CsvHelper;
using FileIOClass.Models;
using System.Globalization;

namespace FileIOClass
{
    public class CsvGuitarFileWriter : IWriter<Guitar>
    {
        private string path = string.Empty;

        public string Path { get => path; set => path = value; }
        public void Append(Guitar entry)
        {
            using var writer = new StreamWriter(Path, true);
            using var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture);
            csvWriter.WriteRecord(entry);
        }

        public void Append(IEnumerable<Guitar> entries)
        {
            using var writer = new StreamWriter(Path, true);
            using var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture);
            csvWriter.WriteRecords(entries);
        }

        public void Clear()
        {
            using var writer = new StreamWriter(Path);
        }

        public void Rewrite(Guitar entry)
        {
            using var writer = new StreamWriter(Path);
            using var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture);
            csvWriter.WriteRecord(entry);
        }

        public void Rewrite(IEnumerable<Guitar> entries)
        {
            using var writer = new StreamWriter(Path);
            using var csvWriter = new CsvWriter(writer, CultureInfo.InvariantCulture);
            csvWriter.WriteRecords(entries);
        }
    }
}
