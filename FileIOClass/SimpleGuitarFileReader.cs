﻿using FileIOClass.Models;

namespace FileIOClass
{
    public class SimpleGuitarFileReader : IReader<Guitar>
    {
        public string Path { get; set; } = string.Empty;

        public IEnumerable<Guitar> GetValues()
        {
            using var reader = new StreamReader(Path);
            string? line;

            while ((line = reader.ReadLine()) != null)
            {
                var guitarLineParts = line.Split(",");
                yield return new Guitar
                {
                    Brand = guitarLineParts[0],
                    Model = guitarLineParts[1],
                    YearManufactured = guitarLineParts[2]
                };
            }
        }
    }
}
