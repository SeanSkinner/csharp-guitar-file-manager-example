﻿using FileIOClass.Models;

namespace FileIOClass
{
    public class SimpleGuitarFileWriter : IWriter<Guitar>
    {
        public string Path { get; set; } = string.Empty;

        public void Append(Guitar entry)
        {
            using var writer = new StreamWriter(Path, true);
            WriteGuitarLineToFile(entry, writer);
        }

        public void Append(IEnumerable<Guitar> entries)
        {
            using var writer = new StreamWriter(Path, true);

            foreach (var entry in entries)
            {
                WriteGuitarLineToFile(entry, writer);
            }
        }

        public void Clear()
        {
            using var writer = new StreamWriter(Path);
        }

        public void Rewrite(Guitar entry)
        {
            using var writer = new StreamWriter(Path);
            WriteGuitarLineToFile(entry, writer);
        }

        private static void WriteGuitarLineToFile(Guitar entry, StreamWriter writer)
        {
            writer.WriteLine($"{entry.Brand},{entry.Model},{entry.YearManufactured}");
        }

        public void Rewrite(IEnumerable<Guitar> entries)
        {
            using var writer = new StreamWriter(Path);

            foreach (var entry in entries)
            {
                WriteGuitarLineToFile(entry, writer);
            }
        }
    }
}
