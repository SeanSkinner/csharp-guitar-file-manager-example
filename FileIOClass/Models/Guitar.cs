﻿namespace FileIOClass.Models
{
    public class Guitar
    {
        public string Brand { get; set; } = string.Empty;
        public string Model { get; set; } = string.Empty;
        public string YearManufactured { get; set; } = string.Empty;
    }
}
